import Vue from 'vue'
import router from './router.js'
import App from './App.vue'
import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css'
import 'typeface-roboto/index.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import TruffleContract from 'truffle-contract'
Object.defineProperty(Vue.prototype, '$TruffleContract', {value: TruffleContract})


Vue.use(Vuetify)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
