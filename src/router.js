import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

/*
// 2. Define route components
const Home = { template: '<div>home</div>' }
const Foo = { template: '<div>foo</div>' }
const Bar = { template: '<div>bar</div>' }
const Unicode = { template: '<div>unicode: {{ $route.params.unicode }}</div>' }
*/


// 3. Create the router
const router = new VueRouter({
  mode: 'hash',
  //base: __dirname,
  routes: [
  //  { path: '/', component: Test }, // all paths are defined without the hash.
    //{ path: '/foo', name: "foo", component: Modal, props: true },
  //  { path: '/bar', component: Test },
    //est{ path: '/é', component: Unicode },
    //{ path: '/é/:unicode', component: Unicode }

  ],
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {selector: to.hash}
    } else {
      if (savedPosition) {
        return savedPosition
      } else {
        return { x: 0, y: 0 }
      }
    }
  },
})

export default router;
