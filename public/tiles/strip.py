#!/usr/bin/python

import csv
import json
import math

with open("scan.log", "rb") as csvfile:
	i = 0
	line = 0
	tiles = []
	
	reader = csv.reader(csvfile, delimiter = ",")
	for row in reader:
		if line % 512 == 0:
			#print i, row
			i += 1;
			tile = {	
				"id": i,
				"lat": float(row[1]),
				"lon": float(row[2]),
				"px": int(row[0])-2,
				"dist": float(row[3]) - 0.029302
			}
			#print tile
			tiles.append(tile)
			outfile = open("../tokens/{}".format(i), "w")	
			json.dump({
  "description": "CryptoBorders US-MX Tiles", 
  "external_url": "https://cryptoborders.io/tokens/".format(i), 
  "image": "https://cryptoborders.io/tiles/{:06d}.jpg".format(i), 
  "name": "#{} @km {:.1f}".format(i, float(row[3]))
			}, outfile, indent=2)
			
		line += 1
	
	outfile = open("tiles.json", "w")	
	json.dump(tiles, outfile, indent=2)
	
	
	

