const CryptoBordersToken = artifacts.require("./CryptoBordersToken.sol");
//const TokenAuction = artifacts.require("./TokenAuction.sol");
const util = require("util");
const fs = require("fs");
const path = require("path");

const promisify = (fn) => {
 return (...args) => {
    return new Promise((resolve, reject)=>{
      fn(...args, function(err, res){
        if(err){
          return reject(err);
        }
        return resolve(res);
      })
    })
  }
}

const writeFile = promisify(fs.writeFile);

module.exports = async function(deployer) {
  const cryptoBordersToken = await deployer.deploy(CryptoBordersToken);
//  const auctionContract = await deployer.deploy(
//    TokenAuction,
//    CryptoBordersToken.address
//  );
  const addresses = {
    tokenAddress: CryptoBordersToken.address,
    //auctionAddress: TokenAuction.address
  };

  await writeFile(
    path.join(__dirname, "..", "build", "addresses.json"),
    JSON.stringify(addresses)
  );
};
