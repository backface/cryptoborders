const HDWalletProvider = require("truffle-hdwallet-provider")
const web3 = require('web3')

const MNEMONIC = process.env.MNEMONIC
const INFURA_KEY = process.env.INFURA_KEY
const OWNER_ADDRESS = process.env.OWNER_ADDRESS
const NETWORK = process.env.NETWORK

const addresses = require('../build/addresses.json')
const NFT_CONTRACT_ADDRESS = addresses.tokenAddress;


if (!MNEMONIC || !INFURA_KEY || !OWNER_ADDRESS || !NETWORK) {
    console.error("Please set a mnemonic, infura key, owner, network, and contract address.")
    return
}

const NFT_ABI = require('../build/contracts/CryptoBordersToken.json')

async function main() {
    console.log('get wallet');
    const provider = new HDWalletProvider(MNEMONIC, `https://${NETWORK}.infura.io/v3/${INFURA_KEY}`)
    console.log('get web3 instance');
    const web3Instance = new web3(
        provider
    )
    console.log(NFT_CONTRACT_ADDRESS);
    if (NFT_CONTRACT_ADDRESS) {
        console.log('get contract');
        const nftContract = new web3Instance.eth.Contract(NFT_ABI.abi, NFT_CONTRACT_ADDRESS, { gasLimit: "5000000" })

        let to_mint = [1, 2, 3, 10, 11, 12]
        console.log('now mint ' + to_mint);

        for (var i in to_mint) {
            const result = await nftContract.methods.claimToken(to_mint[i]).send({ from: OWNER_ADDRESS });
            console.log(result);
            console.log("Minted tile #" + to_mint[i] + " - Transaction: " + result.transactionHash)
        }
    }
}

main()
