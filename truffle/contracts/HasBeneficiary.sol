pragma solidity ^0.5.0;

import "../node_modules/openzeppelin-solidity/contracts/token/ERC721/ERC721.sol";
import '../node_modules/openzeppelin-solidity/contracts/ownership/Ownable.sol';

contract HasBeneficiary is ERC721, Ownable {

    using SafeMath for uint256;

	// initial values

	// donate to 350.org (https://350.org/other-ways-to-give/)
    address payable beneficiary = address(0x50990F09d4f0cb864b8e046e7edC749dE410916b) ;

    // distribute 50%
    uint public weight = 50;

    function transferWithBeneficiarySplit() public payable returns (bool) {
		require(msg.value > 0, "Insufficient ETH sent with transaction");

		address payable owner =  address(uint160(owner()));

		if (beneficiary != address(0)) {
		    beneficiary.transfer(
			    msg.value.mul(weight).div(100)
		    );
		    owner.transfer(
			    msg.value.mul(100-weight).div(100)
			);
		} else {
		    owner.transfer(msg.value);
		}
		return true;
	}

    function setBeneficiary(address _address) public onlyOwner {
        beneficiary = address(uint160(_address));
    }

    function setBeneficiaryWeight(uint256 _weight) public onlyOwner {
        weight = _weight;
    }

}
