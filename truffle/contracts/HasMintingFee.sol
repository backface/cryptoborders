pragma solidity ^0.5.0;

import "../node_modules/openzeppelin-solidity/contracts/token/ERC721/ERC721.sol";
import '../node_modules/openzeppelin-solidity/contracts/ownership/Ownable.sol';


contract HasMintingFee is ERC721, Ownable {

	// initial values
    uint256 public mintingFee = 0.2 ether;

    function mint(address _to, uint256 _tokenID) payable public returns (bool) {
  		require(msg.value >= mintingFee);
  		super._mint(_to, _tokenID);
		    return true;
    }

    function SetMintingFee(uint256 _value) public onlyOwner {
        mintingFee = _value;
    }
}
