	pragma solidity ^0.5.0;

// =============================
/*	CRYPTOBORDERS NFT TOKEN   */
// =============================

import "../node_modules/openzeppelin-solidity/contracts/token/ERC721/ERC721Full.sol";
import '../node_modules/openzeppelin-solidity/contracts/ownership/Ownable.sol';
import './Strings.sol';


contract CryptoBordersToken is ERC721Full, Ownable {
    using SafeMath for uint256;

	// token supply
    uint256 maxTokens = 199;
    uint256 ownerMinted = 0;
    uint256[] public claimedTokens;

	// fancz name for owner
    address payable janitor;
    
    // donation collector
    address payable beneficiary = address(0x9168Ee4766a4E9C6Ff59225aa4Ed819408F7Dbb0);
    uint256 beneficiaryWeight = 50;

	// initial price
    uint initialPrice = 0.2 ether;

	// auctions
    struct Auction {
      address payable seller;
      uint256 price;
    }
    mapping (uint256 => Auction) public tokenIdToAuction;
		uint256[] public tokensForSale;

    // constructor
    constructor() ERC721Full("ȻryptoɃorders", "ȻɃɌ") public {
      janitor = msg.sender;
    }

    function baseTokenURI() public pure returns (string memory) {
      return "http://www.cryptoborders.io/tokens/";
    }

    function() external payable {
    }

    // ==========================
    /*   CLAIM A TOKEN    		*/
    // ==========================

    function claimToken(uint256 _tokenId) payable public returns (uint256) {

      if(msg.sender != janitor) {

        require(msg.value >= initialPrice, "Insufficient ETH sent with transaction");
        require(!_exists(_tokenId), "Token already exists");
        require(_tokenId <= maxTokens,  "not supplied");

        if (beneficiary != address(0)) {
          beneficiary.transfer(initialPrice.mul(beneficiaryWeight).div(100));
          janitor.transfer(initialPrice.mul((100-beneficiaryWeight)).div(100));
        } else {
          janitor.transfer(initialPrice);
        }
      }

      super._mint(msg.sender, _tokenId);
      claimedTokens.push(_tokenId);
    }


    // ==========================
    /*   AUCTIONING     */
    // ==========================

    function createAuction( uint256 _tokenId, uint256 _price ) public {
      require(msg.sender == ownerOf(_tokenId), "Not your tokens");

      Auction memory _auction = Auction({
         seller: msg.sender,
         price: uint256(_price)
      });

      tokenIdToAuction[_tokenId] = _auction;
      tokensForSale.push(_tokenId);
    }

    function buyToken( uint256 _tokenId ) public payable {
      Auction memory auction = tokenIdToAuction[_tokenId];

      require(auction.seller != address(0), "invalid seller");
      require(msg.value >= auction.price, "Insufficient ETH sent with transaction");

      address payable seller = auction.seller;
      uint256 price = auction.price;

      if (beneficiary != address(0)) {
        seller.transfer(price.mul(67).div(100));
        janitor.transfer(price.mul(33).mul(100 - beneficiaryWeight).div(10000));
        beneficiary.transfer(price.mul(33).mul(beneficiaryWeight).div(10000));
      } else {
        seller.transfer(price.mul(67).div(100));
        janitor.transfer(price.mul(33).div(100));
      }

      delete tokenIdToAuction[_tokenId];
      deleteFromTokensForSale(_tokenId);

      super._transferFrom(seller, msg.sender, _tokenId);
    }

    function cancelAuction( uint256 _tokenId ) public {
      Auction memory auction = tokenIdToAuction[_tokenId];
      require(auction.seller == msg.sender);
      delete tokenIdToAuction[_tokenId];
      deleteFromTokensForSale(_tokenId);
    }

    function deleteFromTokensForSale(uint256 _tokenId) internal {
      uint indexToBeDeleted;
      uint len = tokensForSale.length;

      for (uint i=0; i < len; i++) {
        if (tokensForSale[i] == _tokenId) {
          indexToBeDeleted = i;
          break;
        }
      }
      if (indexToBeDeleted < len-1) {
        tokensForSale[indexToBeDeleted] = tokensForSale[len-1];
      }
      tokensForSale.length--;
    }

    function isForSale(uint256 _tokenId) public view returns (uint256) {
        return tokenIdToAuction[_tokenId].price;
    }

    function getTokensForSale() public view returns (uint256[] memory) {
        return tokensForSale;
    }

    // ==========================
    /*  Public Getter Functions   */
    // ==========================

    function tokensOfOwner(address _owner) public view returns (uint256[] memory) {
        return super._tokensOfOwner(_owner);
    }

    function getClaimedTokens() public view returns (uint256[] memory  ) {
        return claimedTokens;
    }

    function getHowManyClaimed() public view returns (uint256) {
        return claimedTokens.length;
    }

    function getOwnerOf(uint256 _tokenId) public view returns (address) {
        if (!_exists(_tokenId)) {
          return address(0);
        } else {
          return ownerOf(_tokenId);
        }
    }

    function tokenURI(uint256 _tokenId) external view returns (string memory) {
      return Strings.strConcat(
         baseTokenURI(),
         Strings.uint2str(_tokenId)
      );
    }

    function getInitialPrice() public view returns (uint256) {
        return initialPrice;
    }


    // ==========================
    /* ADMIN (OWNER) FUNCTION  */
    // ==========================

    function setInitialPrice(uint256 _price) public onlyOwner {
        initialPrice = _price;
    }

    function setBeneficiary(address _address) public onlyOwner {
        beneficiary = address(uint160(_address));
    }

    function setBeneficiaryWeight(uint256 _weight) public onlyOwner {
        beneficiaryWeight = _weight;
    }

}
