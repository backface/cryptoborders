#!/bin/sh

APIKEY="e1xDw7qjDXN5vPFalb58iGXh"

#npm run build 
#npm run transfer

HASH=`ssh man ipfs add -r -Q www/test.cryptoborders.io/`;

#ssh man ipfs name publish $HASH

curl -H "X-Api-Key: e1xDw7qjDXN5vPFalb58iGXh" https://dns.api.gandi.net/api/v5/domains/cryptoborders.io/records


#curl -D- -X PUT -H "Content-Type: application/json" \
#       -H "X-Api-Key: $APIKEY" \
#        -d '{"items": [{"rrset_name": "testnet",
#             "rrset_type": "TXT",
#             "rrset_values": ["dnslink=/ipfs/QmcDbQb4MrLWiFpsfR6s6NHZr3vtVraMUH7Hpev1FJUJNs"]}]}' \
#             "rrset_ttl": 3600,
#       https://dns.api.gandi.net/api/v5/domains/cryptoborders.io/records



curl -D- -XPUT -H "Content-Type: text/plain" \
    -H"X-Api-Key: $APIKEY" \
    --data-binary @- \
    https://dns.api.gandi.net/api/v5/domains/cryptoborders.io/records \
    << EOF
@ 10800 IN SOA ns1.gandi.net. hostmaster.gandi.net. 1558127318 10800 3600 604800 10800
@ 10800 IN A 89.238.67.180
@ 10800 IN MX 10 spool.mail.gandi.net.
@ 10800 IN MX 50 fb.mail.gandi.net.
@ 10800 IN TXT "v=spf1 include:_mailcust.gandi.net ?all"
blog 10800 IN CNAME blogs.vip.gandi.net.
cloud 1800 IN CNAME cloudflare-ipfs.com.
gw 1800 IN CNAME gateway.ipfs.io.
test 1800 IN CNAME cryptoborders.io.
webmail 10800 IN CNAME webmail.gandi.net.
www 10800 IN CNAME cryptoborders.io.
testnet 1800 IN TXT "dnslink=/ipfs/$HASH"
_dnslink 1800 IN TXT "dnslink=/ipfs/$HASH"
_dnslink.cloud 1800 IN TXT "dnslink=/ipfs/$HASH"
_dnslink.cloud.cryptoborders.io 1800 IN TXT "dnslink=/ipfs/$HASH"	
EOF

echo "published $HASH"
