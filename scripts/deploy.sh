#!/bin/sh

npm run build

rsync -avz  \
	-e "ssh" \
	--exclude=deploy.sh \
	./dist/  \
	man:www/www.cryptoborders.io/test
